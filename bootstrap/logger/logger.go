package logger

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"runtime"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
}

func getCallerInfo() (file string, line int, ok bool) {
	_, file, line, ok = runtime.Caller(3)
	return
}

// SetLevel sets the standard entry level
func SetLevel(levelStr string) error {
	level, err := log.ParseLevel(levelStr)
	if err != nil {
		return err
	}

	log.SetLevel(level)
	return nil
}

// SetOutput sets the standard logger output.
func SetOutput(output io.Writer) {
	log.SetOutput(output)
}

// An entry is the final or intermediate logging entry. It contains all
// the fields passed with WithField{,s}. It's finally logged when Debug, Info,
// Warn, Error, Fatal or Panic is called on it. These objects can be reused and
// passed around as much as you wish to avoid field duplication.
func entry() *log.Entry {
	file, line, _ := getCallerInfo()
	return log.WithFields(log.Fields{
		"on": fmt.Sprintf("%s:%d", file, line),
	})
}

func Debug(msg ...interface{}) {
	entry().Debug(msg...)
}

func Info(msg ...interface{}) {
	entry().Info(msg...)
}

func Warn(msg ...interface{}) {
	entry().Warn(msg...)
}

func Error(msg ...interface{}) {
	entry().Error(msg...)
}

func Fatal(msg ...interface{}) {
	entry().Fatal(msg...)
}
