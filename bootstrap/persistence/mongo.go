package persistence

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2"
	"sync"
)

// Mongo defines parameter to create database session
type Mongo struct {
	hosts    []string
	name     string
	username string
	password string
	timeout  time.Duration

	// Session of the mongo db that will be used concurrently
	Session *mgo.Session

	mu *sync.Mutex
}

const (
	StrongMode    = "strong"
	MonotonicMode = "monotonic"
	EventualMode  = "eventual"
)

func NewMongo(hosts []string, name string, username string, password string, timeout int) *Mongo {
	return &Mongo{
		hosts:    hosts,
		name:     name,
		username: username,
		password: password,
		timeout:  time.Duration(timeout) * time.Second,

		mu: &sync.Mutex{},
	}
}

func parseMode(modeStr string) (mgo.Mode, error) {
	switch modeStr {
	case StrongMode:
		return mgo.Strong, nil
	case MonotonicMode:
		return mgo.Monotonic, nil
	case EventualMode:
		return mgo.Eventual, nil
	}
	var m mgo.Mode
	return m, fmt.Errorf("not a valid mgo mode: %s", modeStr)
}

// SetMode changes the consistency mode for the session.
//
// The default mode is Strong.
//
// In the Strong consistency mode reads and writes will always be made to
// the primary server using a unique connection so that reads and writes are
// fully consistent, ordered, and observing the most up-to-date data.
// This offers the least benefits in terms of distributing load, but the
// most guarantees.  See also Monotonic and Eventual.
//
// In the Monotonic consistency mode reads may not be entirely up-to-date,
// but they will always see the history of changes moving forward, the data
// read will be consistent across sequential queries in the same session,
// and modifications made within the session will be observed in following
// queries (read-your-writes).
//
// In practice, the Monotonic mode is obtained by performing initial reads
// on a unique connection to an arbitrary secondary, if one is available,
// and once the first write happens, the session connection is switched over
// to the primary server.  This manages to distribute some of the reading
// load with secondaries, while maintaining some useful guarantees.
//
// In the Eventual consistency mode reads will be made to any secondary in the
// cluster, if one is available, and sequential reads will not necessarily
// be made with the same connection.  This means that data may be observed
// out of order.  Writes will of course be issued to the primary, but
// independent writes in the same Eventual session may also be made with
// independent connections, so there are also no guarantees in terms of
// write ordering (no read-your-writes guarantees either).
//
// The Eventual mode is the fastest and most resource-friendly, but is
// also the one offering the least guarantees about ordering of the data
// read and written.
func (m *Mongo) SetMode(modeStr string) {
	mode, err := parseMode(modeStr)
	if err != nil {
		panic(err)
	}

	m.Session.SetMode(mode, true)
	return
}

// createSession creates database session
func (m *Mongo) createSession() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Set dial timeout
	if m.timeout == 0 {
		m.timeout = 5 * time.Second
	}
	dialInfo := mgo.DialInfo{
		Addrs:    m.hosts,
		Database: m.name,
		Username: m.username,
		Password: m.password,
		Timeout:  m.timeout,
	}

	session, err := mgo.DialWithInfo(&dialInfo)
	if err != nil {
		return fmt.Errorf("failed dial database %s, err: %v", m.name, err)
	}
	m.Session = session

	return nil
}

// GetSession gets database session to use
func (m *Mongo) GetSession() (*mgo.Session, error) {
	if m.Session == nil {
		err := m.createSession()
		if err != nil {
			return nil, err
		}
	}

	newSession := m.Session.Copy()

	err := newSession.Ping()
	if err != nil {
		return nil, err
	}

	return newSession, nil
}
