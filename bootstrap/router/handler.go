package router

import (
	"github.com/urfave/negroni"
	"net/http"
)

type CustomPanicFormatter struct{}

func (c *CustomPanicFormatter) FormatPanicError(rw http.ResponseWriter, r *http.Request, infos *negroni.PanicInformation) {

}
