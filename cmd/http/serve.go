package http

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/spf13/cobra"

	"spacestock/bootstrap/logger"
	"spacestock/bootstrap/persistence"
	"spacestock/config"
	"spacestock/logic/apartment"
	"spacestock/route"
)

const (
	serviceName = "Space Stock"
	bannerInfo  = `
Name: %s
Port: %s
------------------------------------------------------------------------------
`
)

// Server as the http server
type Server struct {
	appConfig   *config.AppConfig
	handlers    map[string]http.HandlerFunc
	middlewares map[string]func(next http.Handler) http.Handler
}

func getHandler(appConfig config.AppConfig) map[string]http.HandlerFunc {
	// Set event mongo database
	eventDB := persistence.NewMongo(appConfig.Database.SpaceStock.Host, appConfig.Database.SpaceStock.Name, appConfig.Database.SpaceStock.Username, appConfig.Database.SpaceStock.Password, appConfig.Database.SpaceStock.Timeout)
	_, err := eventDB.GetSession()
	if err != nil {
		panic(err)
	}

	apartmentRepository := apartment.NewApartmentRepository(appConfig, *eventDB)

	if appConfig.Database.SpaceStock.Mode != "" {
		eventDB.SetMode(appConfig.Database.SpaceStock.Mode)
	}

	healthHandler := apartment.NewHandler(apartmentRepository)

	// Set handlers
	handlers := make(map[string]http.HandlerFunc)
	handlers[apartment.PostApartmentOperation] = healthHandler.Post
	handlers[apartment.GetAllApartmentOperation] = healthHandler.GetAll
	handlers[apartment.PutApartmentOperation] = healthHandler.Put
	handlers[apartment.DeleteApartmentOperation] = healthHandler.Delete

	return handlers
}

// NewHTTPServer creates new https server
func NewHTTPServer(appConfig *config.AppConfig) *Server {

	return &Server{
		handlers:  getHandler(*appConfig),
		appConfig: appConfig,
	}
}

func printBannerInfo(port string) {
	fmt.Printf(bannerInfo, serviceName, port)
}

// Run runs server
func (s *Server) Run(cmd *cobra.Command, args []string) {

	var gracefulTimeout time.Duration
	gracefulTimeout = time.Duration(s.appConfig.App.GracefulTimeout) * time.Second
	if gracefulTimeout == 0 {
		gracefulTimeout = 30 * time.Second
	}

	// Set router
	route := route.NewRoute(s.handlers)
	handlers, err := route.Route()
	if err != nil {
		logger.Fatal(err)
	}

	// Run server
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.appConfig.App.Port),
		Handler: handlers,
	}

	printBannerInfo(server.Addr)
	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	// Graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeout)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		log.Println(err)
	}
	log.Println("shutting down")
	os.Exit(0)
}
