package cmd

import (
	"github.com/afex/hystrix-go/hystrix"
	"github.com/spf13/cobra"

	"spacestock/bootstrap/logger"
	"spacestock/cmd/http"
	"spacestock/config"
)

var (
	appConfig        *config.AppConfig
	rootCmd          cobra.Command
	cfgTypeFlag      string
	cfgFilePathFlag  string
	cfgConsulKeyFlag string
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgTypeFlag, "config-type", "", "config type (default is file, other ex is consul)")
	rootCmd.PersistentFlags().StringVar(&cfgFilePathFlag, "config-file-path", "", "config file path (default is env.yaml)")
}

// initConfig loads the configuration to appCtx
func initConfig() {
	var err error
	appConfig = config.NewAppConfig()

	err = logger.SetLevel("debug")
	if err != nil {
		panic(err)
	}

	err = appConfig.LoadFromFile(cfgConsulKeyFlag)

	if err != nil {
		panic(err)
	}

	hystrix.Configure(config.HystrixConfigurations)
}

func Execute() {
	var (
		cmdServeHTTP = &cobra.Command{
			Use:   "serve",
			Short: "RunProcessUploaded this application as a server for Apartment that runs in HTTP.",
			Run: func(cmd *cobra.Command, args []string) {
				httpServer := http.NewHTTPServer(appConfig)
				httpServer.Run(cmd, args)
			},
		}
	)

	rootCmd.AddCommand(cmdServeHTTP)
	err := rootCmd.Execute()
	if err != nil {
		logger.Fatal(err)
	}
}
