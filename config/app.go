package config

import (
	"bytes"
	"fmt"
	"github.com/hashicorp/consul/api"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type AppConfig struct {
	App struct {
		Name            string `yaml:"name"`
		Port            int    `yaml:"port"`
		Env             string `yaml:"env"`
		GracefulTimeout int    `yaml:"graceful_timeout"`
	} `yaml:"app"`
	Database struct {
		SpaceStock struct {
			Host           []string `yaml:"host"`
			Name           string   `yaml:"name"`
			Username       string   `yaml:"username"`
			Password       string   `yaml:"password"`
			Timeout        int      `yaml:"timeout"`
			CollectionName string   `yaml:"collection_name"`
			Mode           string   `yaml:"mode"`
		} `yaml:"space_stock"`
	} `yaml:"database"`
}

func NewAppConfig() *AppConfig {
	return &AppConfig{}
}

func (c *AppConfig) LoadFromFile(path string) error {
	if path == "" {
		path = "config/app.yaml"
	}

	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("unable open config file on: %s", path)
	}

	var buff = bytes.NewBuffer([]byte{})

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	buff.Write(data)
	if err := yaml.Unmarshal(buff.Bytes(), c); err != nil {
		return err
	}

	return nil
}

func (c *AppConfig) LoadFromConsul(key string) error {
	// Get a new client
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		panic(err)
	}

	// Get a handle to the KV API
	kv := client.KV()

	// Lookup the pair
	pair, _, err := kv.Get(key, nil)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(pair.Value, c); err != nil {
		return err
	}

	return nil
}
