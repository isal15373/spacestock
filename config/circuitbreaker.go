package config

import (
	"github.com/afex/hystrix-go/hystrix"
)

var HystrixConfigurations = map[string]hystrix.CommandConfig{}
