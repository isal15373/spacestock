#!/bin/bash
echo ">>> Running setup"

echo ">>> Checking running container"
IS_RUNNING=`docker-compose ps -q`
if [[ "$IS_RUNNING" != "" ]]; then
    echo ">>> Take down running container"
    docker-compose down
fi

echo ">>> Up container"
docker-compose up -d

echo ">>> Setup mongodb replication sets"
sleep 15
docker-compose exec space-stock sh -c "mongo --port 27017 < scripts/mongod-space-stock.js"

echo ">>> Create new mongodb users"
sleep 15
docker-compose exec space-stock sh -c "mongo --port 27017 < scripts/mongod-space-stock"