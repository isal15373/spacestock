package apartment

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)


type Apartment struct {
	ID          primitive.ObjectID `bson:"_id" json:"_id,omitempty"`
	Name        string             `bson:"id" json:"name" valid:"required"`
	Image       string             `bson:"image" json:"image" valid:"required"`
	Description string             `bson:"description" json:"description" valid:"required"`
	Address     string             `bson:"address" json:"address" valid:"required"`
	Longitude   string             `bson:"longitude" json:"longitude" valid:"required"`
	Latitude    string             `bson:"latitude" json:"latitude" valid:"required"`
}
