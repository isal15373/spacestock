package apartment

import (
	"encoding/json"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"github.com/opentracing/opentracing-go"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"spacestock/bootstrap/logger"
	"spacestock/utility/response"
)

const (
	// PostApartmentOperation is const apartment.post
	PostApartmentOperation = "apartment.PostOperation"

	// PutApartmentOperation is const apartment.put
	PutApartmentOperation = "apartment.PutOperation"

	// GetAllApartmentOperation is const apartment.get_all
	GetAllApartmentOperation = "apartment.GetAllOperation"

	// DeleteApartmentOperation is const apartment.delete
	DeleteApartmentOperation = "apartment.DeleteOperation"
)

// Handler is apartment for health check purpose
type Handler struct {
	apartmentRepository IApartmentRepository
}

// NewHandler create new health check apartment
func NewHandler(apartmentRepository IApartmentRepository) *Handler {
	return &Handler{
		apartmentRepository: apartmentRepository,
	}
}

// Post is apartment function for ping-ing the API
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {
	span, ctx := opentracing.StartSpanFromContext(r.Context(), PostApartmentOperation)
	defer span.Finish()

	// payload response
	var payload Apartment
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	// validate payload
	isValid, err := govalidator.ValidateStruct(payload)
	if !isValid {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	payload.ID = primitive.NewObjectID()
	err = h.apartmentRepository.Save(ctx, payload)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusInternalServerError, nil)
		return
	}

	response.WriteApplicationJson(w, http.StatusCreated, nil)
	return
}

// GetAll is to get all data apartment function
func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	span, ctx := opentracing.StartSpanFromContext(r.Context(), GetAllApartmentOperation)
	defer span.Finish()

	apartments, err := h.apartmentRepository.GetAll(ctx)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusInternalServerError, nil)
		return
	}

	response.WriteApplicationJson(w, http.StatusOK, apartments)
	return
}

// Put is update apartment function
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {
	span, ctx := opentracing.StartSpanFromContext(r.Context(), PutApartmentOperation)
	defer span.Finish()

	params := mux.Vars(r)

	// payload response
	var payload Apartment
	ID, err := primitive.ObjectIDFromHex(params["ID"])
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	// validate payload
	isValid, err := govalidator.ValidateStruct(payload)
	if !isValid {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	err = h.apartmentRepository.Update(ctx, ID, payload)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusInternalServerError, nil)
		return
	}

	response.WriteApplicationJson(w, http.StatusNoContent, nil)
	return
}

// Delete is to delete data apartment function
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	span, ctx := opentracing.StartSpanFromContext(r.Context(), DeleteApartmentOperation)
	defer span.Finish()

	params := mux.Vars(r)

	// payload response
	ID, err := primitive.ObjectIDFromHex(params["ID"])
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusBadRequest, nil)
		return
	}

	err = h.apartmentRepository.Delete(ctx, ID)
	if err != nil {
		logger.Debug(err)
		response.WriteApplicationJson(w, http.StatusInternalServerError, nil)
		return
	}

	response.WriteApplicationJson(w, http.StatusNoContent, nil)
	return
}
