package apartment

import (
	"context"
	"errors"
	"github.com/gorilla/mux"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var simulateError = errors.New("simulate error")

// apartmentRepositoryFake is the interface for `ApartmentRepositoryFake`
type apartmentRepositoryFake struct {
	testSave   bool
	testGetAll bool
	testUpdate bool
	testDelete bool
}

func (a *apartmentRepositoryFake) Save(ctx context.Context, apartment Apartment) error {
	if a.testSave {
		return nil
	}
	return simulateError
}

func (a *apartmentRepositoryFake) Update(ctx context.Context, ID primitive.ObjectID, apartment Apartment) error {
	if a.testUpdate {
		return nil
	}
	return simulateError
}

func (a *apartmentRepositoryFake) Delete(ctx context.Context, ID primitive.ObjectID) error {
	if a.testDelete {
		return nil
	}
	return simulateError
}

func (a *apartmentRepositoryFake) Get(ctx context.Context, ID primitive.ObjectID) (apartment Apartment, err error) {
	return apartment, nil
}

func (a *apartmentRepositoryFake) GetAll(ctx context.Context) (apartments []Apartment, err error) {
	if a.testGetAll {
		return apartments, nil
	}
	return apartments, simulateError
}

func Test_PostApartment(t *testing.T) {
	// Initiate health service
	apartmentRepository := apartmentRepositoryFake{}
	handler := NewHandler(&apartmentRepository)

	t.Run("Failed to decode while Save", func(t *testing.T) {
		payload := `can't decode`
		req := httptest.NewRequest(http.MethodPost, "/space-stock/ex/apartment", strings.NewReader(payload))
		rw := httptest.NewRecorder()
		handler.Post(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Failed to validate while Save", func(t *testing.T) {
		payload := `{"name":"","image":"","description":"","address":"","longitude":"","latitude":""}`
		req := httptest.NewRequest(http.MethodPost, "/space-stock/ex/apartment", strings.NewReader(payload))
		rw := httptest.NewRecorder()
		handler.Post(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Save to mongo error while Save", func(t *testing.T) {
		payload := `{"name":"soho","image":"http://image.jpeg","description":"disewa","address":"jaksel","longitude":"100","latitude":"200"}`
		req := httptest.NewRequest(http.MethodPost, "/space-stock/ex/apartment", strings.NewReader(payload))
		rw := httptest.NewRecorder()
		handler.Post(rw, req)

		assert.Equal(t, http.StatusInternalServerError, rw.Code)
	})

	apartmentRepository.testSave = true
	handler = NewHandler(&apartmentRepository)
	t.Run("Success", func(t *testing.T) {
		payload := `{"name":"soho","image":"http://image.jpeg","description":"disewa","address":"jaksel","longitude":"100","latitude":"200"}`
		req := httptest.NewRequest(http.MethodPost, "/space-stock/ex/apartment", strings.NewReader(payload))
		rw := httptest.NewRecorder()
		handler.Post(rw, req)

		assert.Equal(t, http.StatusCreated, rw.Code)
	})
}

func Test_GetAllApartment(t *testing.T) {
	// Initiate health service
	apartmentRepository := apartmentRepositoryFake{}
	handler := NewHandler(&apartmentRepository)

	t.Run("Failed to get data while Update", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/space-stock/ex/apartment", nil)
		rw := httptest.NewRecorder()
		handler.GetAll(rw, req)

		assert.Equal(t, http.StatusInternalServerError, rw.Code)
	})

	apartmentRepository.testGetAll = true
	handler = NewHandler(&apartmentRepository)
	t.Run("Success", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/space-stock/ex/apartment", nil)
		rw := httptest.NewRecorder()
		handler.GetAll(rw, req)

		assert.Equal(t, http.StatusOK, rw.Code)
	})
}

func Test_PutApartment(t *testing.T) {
	// Initiate health service
	apartmentRepository := apartmentRepositoryFake{}
	handler := NewHandler(&apartmentRepository)

	t.Run("Failed to get ObjectIDFromHex while Delete", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodPut, "/space-stock/ex/apartment/", nil)
		rw := httptest.NewRecorder()
		handler.Put(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Failed to decode while Update", func(t *testing.T) {
		payload := `can't decode`
		req := httptest.NewRequest(http.MethodPut, "/space-stock/ex/apartment", strings.NewReader(payload))
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Put(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Failed to validate while Update", func(t *testing.T) {
		payload := `{"name":"","image":"","description":"","address":"","longitude":"","latitude":""}`
		req := httptest.NewRequest(http.MethodPut, "/space-stock/ex/apartment", strings.NewReader(payload))
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Put(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Save to mongo error while Update", func(t *testing.T) {
		payload := `{"name":"soho","image":"http://image.jpeg","description":"disewa","address":"jaksel","longitude":"100","latitude":"200"}`
		req := httptest.NewRequest(http.MethodPut, "/space-stock/ex/apartment", strings.NewReader(payload))
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Put(rw, req)

		assert.Equal(t, http.StatusInternalServerError, rw.Code)
	})

	apartmentRepository.testUpdate = true
	handler = NewHandler(&apartmentRepository)
	t.Run("Success", func(t *testing.T) {
		payload := `{"name":"soho","image":"http://image.jpeg","description":"disewa","address":"jaksel","longitude":"100","latitude":"200"}`
		req := httptest.NewRequest(http.MethodPut, "/space-stock/ex/apartment", strings.NewReader(payload))
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Put(rw, req)

		assert.Equal(t, http.StatusNoContent, rw.Code)
	})
}

func Test_DeleteApartment(t *testing.T) {
	// Initiate health service
	apartmentRepository := apartmentRepositoryFake{}
	handler := NewHandler(&apartmentRepository)

	t.Run("Failed to get ObjectIDFromHex while Delete", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodDelete, "/space-stock/ex/apartment/", nil)
		rw := httptest.NewRecorder()
		handler.Delete(rw, req)

		assert.Equal(t, http.StatusBadRequest, rw.Code)
	})

	t.Run("Failed while Save", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodDelete, "/space-stock/ex/apartment/{ID}", nil)
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Delete(rw, req)

		assert.Equal(t, http.StatusInternalServerError, rw.Code)
	})

	apartmentRepository.testDelete = true
	handler = NewHandler(&apartmentRepository)
	t.Run("Success", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodDelete, "/space-stock/ex/apartment{ID}", nil)
		req = mux.SetURLVars(req, map[string]string{"ID": "5e6c65568d83f48c364c7125"})
		rw := httptest.NewRecorder()
		handler.Delete(rw, req)

		assert.Equal(t, http.StatusNoContent, rw.Code)
	})
}