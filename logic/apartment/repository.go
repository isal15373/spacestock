package apartment

import (
	"context"
	
	"spacestock/bootstrap/persistence"
	"spacestock/config"

	"github.com/opentracing/opentracing-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	// Operation name
	SaveOperation   = "repository.save"
	GetAllOperation = "repository.get_all"
	GetOperation    = "repository.get"
	UpdateOperation = "repository.update"
	DeleteOperation = "repository.delete"
)

// IApartmentRepository is the interface for `ApartmentRepository`
type IApartmentRepository interface {
	Save(ctx context.Context, apartment Apartment) error
	Update(ctx context.Context, ID primitive.ObjectID, apartment Apartment) error
	Delete(ctx context.Context, ID primitive.ObjectID) error
	Get(ctx context.Context, ID primitive.ObjectID) (apartments Apartment, err error)
	GetAll(ctx context.Context) (apartments []Apartment, err error)
}

// ApartmentRepository is the struct that act as the service for submission event
type Repository struct {
	db             persistence.Mongo
	dbName         string
	collectionName string
}

// NewApartmentRepository
func NewApartmentRepository(appConfig config.AppConfig, db persistence.Mongo) *Repository {
	return &Repository{
		db:             db,
		dbName:         appConfig.Database.SpaceStock.Name,
		collectionName: appConfig.Database.SpaceStock.CollectionName,
	}
}

// Save data
func (r *Repository) Save(ctx context.Context, apartment Apartment) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, SaveOperation)
	defer span.Finish()

	// Get Session
	session, err := r.db.GetSession()
	if err != nil {
		return err
	}
	defer session.Close()

	err = session.DB(r.dbName).C(r.collectionName).Insert(&apartment)
	if err != nil {
		return err
	}

	return nil
}

// GetAll data
func (r *Repository) GetAll(ctx context.Context) (apartments []Apartment, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, GetAllOperation)
	defer span.Finish()

	// Get Session
	session, err := r.db.GetSession()
	if err != nil {
		return apartments, err
	}
	defer session.Close()

	err = session.DB(r.dbName).C(r.collectionName).Find(nil).All(&apartments)
	if err != nil {
		return apartments, err
	}

	return apartments, nil
}

// Get data
func (r *Repository) Get(ctx context.Context, ID primitive.ObjectID) (apartment Apartment, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, GetOperation)
	defer span.Finish()

	// Get Session
	session, err := r.db.GetSession()
	if err != nil {
		return apartment, err
	}
	defer session.Close()

	err = session.DB(r.dbName).C(r.collectionName).FindId(ID).One(apartment)
	if err != nil {
		return apartment, err
	}

	return apartment, nil
}

// Update data
func (r *Repository) Update(ctx context.Context, ID primitive.ObjectID, apartment Apartment) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, UpdateOperation)
	defer span.Finish()

	// Get Session
	session, err := r.db.GetSession()
	if err != nil {
		return err
	}
	defer session.Close()

	err = session.DB(r.dbName).C(r.collectionName).UpdateId(ID, apartment)
	if err != nil {
		return err
	}

	return nil
}

// Delete data
func (r *Repository) Delete(ctx context.Context, ID primitive.ObjectID) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, DeleteOperation)
	defer span.Finish()

	// Get Session
	session, err := r.db.GetSession()
	if err != nil {
		return err
	}
	defer session.Close()

	err = session.DB(r.dbName).C(r.collectionName).RemoveId(ID)
	if err != nil {
		return err
	}

	return nil
}
