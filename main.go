package main

import (
	"spacestock/cmd"
)

func main() {
	cmd.Execute()
}
