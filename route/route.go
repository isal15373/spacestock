package route

import (
	"fmt"
	"net/http"

	"spacestock/bootstrap/logger"
	"spacestock/bootstrap/router"
	"spacestock/logic/apartment"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Router is the interface for Route method
type Router interface {
	Route() (http.Handler, error)
}

type Route struct {
	handlers map[string]http.HandlerFunc
	mw       map[string]func(next http.Handler) http.Handler
}

// Route routes the api request
func (r *Route) Route() (http.Handler, error) {
	muxRouter := mux.NewRouter()

	// V5 route
	v5 := muxRouter.PathPrefix("/space-stock").Subrouter()
	r.internalRoute(v5.PathPrefix("/ex").Subrouter())

	// Recovery apartment
	n := negroni.New()
	n.Use(newRecovery())
	n.UseHandler(muxRouter)

	return n, nil
}

// internalRoute defines all route for handling internal request.
// This routes will be accessed by private network or in the same vpc
func (r *Route) internalRoute(router *mux.Router) {
	// Health
	router.HandleFunc("/apartment", r.handlers[apartment.PostApartmentOperation]).Methods(http.MethodPost)
	router.HandleFunc("/apartment", r.handlers[apartment.GetAllApartmentOperation]).Methods(http.MethodGet)
	router.HandleFunc("/apartment/{ID}", r.handlers[apartment.PutApartmentOperation]).Methods(http.MethodPut)
	router.HandleFunc("/apartment/{ID}", r.handlers[apartment.DeleteApartmentOperation]).Methods(http.MethodDelete)
}

func newRecovery() *negroni.Recovery {
	recovery := negroni.NewRecovery()
	recovery.PanicHandlerFunc = recoveryHandler
	recovery.PrintStack = false
	recovery.Formatter = &router.CustomPanicFormatter{}

	return recovery
}

func recoveryHandler(information *negroni.PanicInformation) {
	logger.Error(fmt.Errorf("panic: %v", information.RecoveredPanic))
}

// NewRoute creates a new route v5
func NewRoute(handlers map[string]http.HandlerFunc) *Route {
	return &Route{
		handlers: handlers,
	}
}
