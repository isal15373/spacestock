package response

import (
	"encoding/json"
	"net/http"

	"spacestock/bootstrap/logger"
)

func WriteApplicationJson(w http.ResponseWriter, httpCode int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	err := json.NewEncoder(w).Encode(body)
	if err != nil {
		logger.Error(err)
	}

	return
}
